#!/bin/sh

SCRIPT_DIR=$(dirname $0)
DIR="$SCRIPT_DIR/.."

cat $DIR/sql/create_db.sql \
    $DIR/sql/fill_dir.sql \
    $DIR/sql/fill_test_data.sql |
        sqlite3 $DIR/db/test.db
