#!/usr/bin/env perl

use Modern::Perl;
use Crypt::OpenSSL::Random;
use MIME::Base64;

# Base64 encoded salt
print encode_base64(Crypt::OpenSSL::Random::random_bytes(16));

# Base64 encoded secret
print encode_base64(Crypt::OpenSSL::Random::random_bytes(32));
