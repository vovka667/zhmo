#!/usr/bin/env perl 

use strict;
use warnings;
use utf8;

use Getopt::Long;
use Crypt::Eksblowfish::Bcrypt qw/bcrypt_hash/;
use MIME::Base64;

my ($password, $salt);

GetOptions ("password=s" => \$password,
            "salt=s" => \$salt)
    or die("Error in command line arguments\n");

$salt = decode_base64($salt);

print encode_base64(bcrypt_hash(
    {
        key_nul => 1,
        cost    => 8,
        salt    => $salt,
    },
    $password,
));
