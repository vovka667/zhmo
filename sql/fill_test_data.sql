-- Пользователи -- password = base64(bcrypt('q1', $salt))
INSERT OR REPLACE INTO users (
        id,
        email,
        password,
        salt,
        first_name,
        last_name,
        tz
    ) VALUES (
        1,
        'root',
        '+VAVLDRdb1S0H4X4aufQZ1lIacWsOsE=',
        'zrOUZOkUEtPjuL/r25bj0A==',
        'Vladimir',
        'Indik',
        'Europe/Moscow'
);
INSERT OR REPLACE INTO users (
        id,
        email,
        password,
        salt,
        first_name,
        last_name,
        tz
    ) VALUES (
        2,
        'toor',
        '+VAVLDRdb1S0H4X4aufQZ1lIacWsOsE=',
        'zrOUZOkUEtPjuL/r25bj0A==',
        'Indik',
        'Vladimir',
        'Europe/Moscow'
);

-- Счета
INSERT OR REPLACE INTO accounts (id, name, account_type_id, balance, user_id, currency_id)
    VALUES (1, 'Зарплатная карта', 1, 1500000, 1, 3);
INSERT OR REPLACE INTO accounts (id, name, account_type_id, balance, user_id, currency_id)
    VALUES (2, 'Заначка', 3, 1500000000, 1, 2);
INSERT OR REPLACE INTO accounts (id, name, account_type_id, balance, user_id, currency_id)
    VALUES (3, 'Зарплата', 4, 0, 1, 3);
INSERT OR REPLACE INTO accounts (id, name, account_type_id, balance, user_id, currency_id)
    VALUES (4, 'Зарплатная карта', 1, 1500000, 2, 3);

-- Контроль доступа
INSERT OR REPLACE INTO acl (id, user_id, account_id, read, write)
    VALUES (1, 1, 1, 1, 1);
INSERT OR REPLACE INTO acl (id, user_id, account_id, read, write)
    VALUES (2, 1, 2, 1, 1);
INSERT OR REPLACE INTO acl (id, user_id, account_id, read, write)
    VALUES (3, 1, 3, 1, 1);
INSERT OR REPLACE INTO acl (id, user_id, account_id, read, write)
    VALUES (4, 2, 4, 1, 1);

-- Операции
INSERT OR REPLACE INTO transactions (
        debit_id,
        d_sum,
        credit_id,
        c_sum,
        transaction_type_id,
        time,
        descr
    ) VALUES (
        1,
        100,
        2,
        100,
        2,
        1,
        '10% На покупку квартиры'
);
INSERT OR REPLACE INTO transactions (
        debit_id,
        d_sum,
        credit_id,
        c_sum,
        transaction_type_id,
        time,
        descr
    ) VALUES (
        3,
        1000,
        1,
        1000,
        1,
        0,
        'Пришла зпшечка за 70й год'
);
