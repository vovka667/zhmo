-- Валюты
INSERT OR REPLACE INTO currencies (id, name, iso, sign)
    VALUES (1, 'Доллар США', 'USD', '$');
INSERT OR REPLACE INTO currencies (id, name, iso, sign)
    VALUES (2, 'Евро', 'EUR', '€');
INSERT OR REPLACE INTO currencies (id, name, iso, sign)
    VALUES (3, 'Российский рубль', 'RUB', 'р.');

-- Типы счетов
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (1, 'Дебетовая карта', 0);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (2, 'Наличные', 0);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (3, 'Депозит', 0);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (4, 'Зарплата', 1);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (5, 'Коммунальные платежи', 1);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (6, 'Актив', 1);
INSERT OR REPLACE INTO account_types (id, name, external)
    VALUES (7, 'Пасив', 1);

-- Типы операций
INSERT OR REPLACE INTO transaction_types (id, name)
    VALUES (1, 'Зарплата');
INSERT OR REPLACE INTO transaction_types (id, name)
    VALUES (2, 'Накопление');
