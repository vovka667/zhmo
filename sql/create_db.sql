PRAGMA foreign_keys = ON;
PRAGMA encoding = "UTF-8";

-- Данные
-- Операции (проводки)
CREATE TABLE IF NOT EXISTS transactions (
    id                      INTEGER PRIMARY KEY ASC,
    debit_id                INTEGER NOT NULL,
    d_sum                   INTEGER NOT NULL,
    credit_id               INTEGER NOT NULL,
    c_sum                   INTEGER NOT NULL,
    transaction_type_id     INTEGER NOT NULL,
    time                    INTEGER NOT NULL,
    descr                   TEXT NOT NULL,
    FOREIGN KEY(debit_id)   REFERENCES accounts(id),
    FOREIGN KEY(credit_id)  REFERENCES accounts(id),
    FOREIGN KEY(transaction_type_id)
                            REFERENCES transaction_types(id)
);

CREATE INDEX IF NOT EXISTS transactions_debit  ON transactions(debit_id);
-- CREATE INDEX IF NOT EXISTS transactions_time   ON transactions(time);
CREATE INDEX IF NOT EXISTS transactions_credit ON transactions(credit_id);

CREATE TRIGGER IF NOT EXISTS update_c_sum UPDATE OF c_sum ON transactions
    BEGIN
        UPDATE accounts
            SET balance = balance + new.c_sum - old.c_sum
            WHERE id = old.credit_id;
    END;
CREATE TRIGGER IF NOT EXISTS update_d_sum UPDATE OF d_sum ON transactions
    BEGIN
        UPDATE accounts
            SET balance = balance - new.c_sum + old.c_sum
            WHERE id = new.debit_id;
    END;
CREATE TRIGGER IF NOT EXISTS delete_transaction DELETE ON transactions
    BEGIN
        UPDATE accounts
            SET balance = balance - old.c_sum
            WHERE id = old.credit_id;
        UPDATE accounts
            SET balance = balance + old.d_sum
            WHERE id = old.debit_id;
    END;

-- Счета
CREATE TABLE IF NOT EXISTS accounts (
    id                      INTEGER PRIMARY KEY ASC,
    name                    TEXT NOT NULL,
    account_type_id         INTEGER NOT NULL,
    balance                 INTEGER NOT NULL,
    user_id                 INTEGER NOT NULL,
    currency_id             INTEGER NOT NULL,
    FOREIGN KEY(account_type_id)
                            REFERENCES account_types(id),
    FOREIGN KEY(user_id)    REFERENCES users(id),
    FOREIGN KEY(currency_id)
                            REFERENCES currencies(id)
);

-- Контроль доступа
CREATE TABLE IF NOT EXISTS acl (
    id                      INTEGER PRIMARY KEY ASC,
    user_id                 INTEGER NOT NULL,
    account_id              INTEGER NOT NULL,
    read                    INTEGER NOT NULL, -- BOOLEAN
    write                   INTEGER NOT NULL, -- BOOLEAN
    FOREIGN KEY(user_id)    REFERENCES users(id),
    FOREIGN KEY(account_id) REFERENCES accounts(id)
);

-- CREATE INDEX IF NOT EXISTS acl_account    ON acl(account_id);
-- CREATE INDEX IF NOT EXISTS acl_user_read  ON acl(user_id, read);
-- CREATE INDEX IF NOT EXISTS acl_user_write ON acl(user_id, write);
CREATE INDEX IF NOT EXISTS acl_user ON acl(user_id);
 
-- Пользователи
CREATE TABLE IF NOT EXISTS users (
    id                      INTEGER PRIMARY KEY ASC,
    email                   TEXT NOT NULL,
    password                TEXT NOT NULL,
    salt                    TEXT NOT NULL,
    first_name              TEXT NOT NULL,
    last_name               TEXT NOT NULL,
    tz                      TEXT NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS users_email ON users(email);

-- Справочники
-- Типы счетов
CREATE TABLE IF NOT EXISTS account_types (
    id                      INTEGER PRIMARY KEY ASC,
    name                    TEXT NOT NULL,
    external                INTEGER NOT NULL -- BOOLEAN
);

-- Типы операций
CREATE TABLE IF NOT EXISTS transaction_types (
    id                      INTEGER PRIMARY KEY ASC,
    name                    TEXT NOT NULL
);

-- Валюты
CREATE TABLE IF NOT EXISTS currencies (
    id                      INTEGER PRIMARY KEY ASC,
    name                    TEXT NOT NULL,
    iso                     TEXT NOT NULL, -- ISO 4217
    sign                    TEXT NOT NULL
);
