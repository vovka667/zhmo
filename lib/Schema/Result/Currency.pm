package Schema::Result::Currency;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Schema::Result::Currency

=cut

__PACKAGE__->table("currencies");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 iso

  data_type: 'text'
  is_nullable: 0

=head2 sign

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "iso",
  { data_type => "text", is_nullable => 0 },
  "sign",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 accounts

Type: has_many

Related object: L<Schema::Result::Account>

=cut

__PACKAGE__->has_many(
  "accounts",
  "Schema::Result::Account",
  { "foreign.currency_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-07 16:20:11
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BzeWSZeJi7wUrMSjfGTaRA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
