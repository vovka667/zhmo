package Schema::Result::Account;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Schema::Result::Account

=cut

__PACKAGE__->table("accounts");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 account_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 balance

  data_type: 'integer'
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 currency_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "account_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "balance",
  { data_type => "integer", is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "currency_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 transactions_credits

Type: has_many

Related object: L<Schema::Result::Transaction>

=cut

__PACKAGE__->has_many(
  "transactions_credits",
  "Schema::Result::Transaction",
  { "foreign.credit_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 transactions_debits

Type: has_many

Related object: L<Schema::Result::Transaction>

=cut

__PACKAGE__->has_many(
  "transactions_debits",
  "Schema::Result::Transaction",
  { "foreign.debit_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 currency

Type: belongs_to

Related object: L<Schema::Result::Currency>

=cut

__PACKAGE__->belongs_to(
  "currency",
  "Schema::Result::Currency",
  { id => "currency_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 user

Type: belongs_to

Related object: L<Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 account_type

Type: belongs_to

Related object: L<Schema::Result::AccountType>

=cut

__PACKAGE__->belongs_to(
  "account_type",
  "Schema::Result::AccountType",
  { id => "account_type_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 acls

Type: has_many

Related object: L<Schema::Result::Acl>

=cut

__PACKAGE__->has_many(
  "acls",
  "Schema::Result::Acl",
  { "foreign.account_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-09 20:20:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kVTXSkHD3hYjaoV72zrn/g

__PACKAGE__->has_many(
  "acls",
  "Schema::Result::Acl",
  { "foreign.account_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0, join_type => '' },
);

1;
