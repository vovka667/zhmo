package Schema::Result::Transaction;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Schema::Result::Transaction

=cut

__PACKAGE__->table("transactions");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 debit_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 d_sum

  data_type: 'integer'
  is_nullable: 0

=head2 credit_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 c_sum

  data_type: 'integer'
  is_nullable: 0

=head2 transaction_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 time

  data_type: 'integer'
  is_nullable: 0

=head2 descr

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "debit_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "d_sum",
  { data_type => "integer", is_nullable => 0 },
  "credit_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "c_sum",
  { data_type => "integer", is_nullable => 0 },
  "transaction_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "time",
  { data_type => "integer", is_nullable => 0 },
  "descr",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 transaction_type

Type: belongs_to

Related object: L<Schema::Result::TransactionType>

=cut

__PACKAGE__->belongs_to(
  "transaction_type",
  "Schema::Result::TransactionType",
  { id => "transaction_type_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 credit

Type: belongs_to

Related object: L<Schema::Result::Account>

=cut

__PACKAGE__->belongs_to(
  "credit",
  "Schema::Result::Account",
  { id => "credit_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 debit

Type: belongs_to

Related object: L<Schema::Result::Account>

=cut

__PACKAGE__->belongs_to(
  "debit",
  "Schema::Result::Account",
  { id => "debit_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-09 20:20:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LFppA0u9ndI5ZD1H2drN2w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
