package Schema::ResultSet::Currency;

use Modern::Perl;
use base 'DBIx::Class::ResultSet';

sub all {
    my $self = shift;

    my $rs = $self->search(
        undef,
        {
            columns      => [ qw/ id name / ],
            result_class => 'DBIx::Class::ResultClass::HashRefInflator',
        },
    );

    my $result = {};

    while (my $row = $rs->next) {
        $result->{$row->{id}} = $row->{name};
    }

    return $result;
}

1;
