package Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-01-07 16:20:11
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vnsscRETT3oPUQnGauiVig

#TODO вынести это нахрен отсюда
sub format_num {
    my $num       = pop;
    my ($symbol)  = $num =~ /^(-)/;
    my $int       = substr $num, 0, -2;
    my $hundredth = substr $num, -2;

    $int = reverse ($int);

    my @digits = $int =~ m/\d{1,3}/g;
    $int = join (" ", @digits);

    $int = reverse ($int);

    return sprintf("%s%s,%02d", $symbol // '', $int || 0, $hundredth);
}

1;
