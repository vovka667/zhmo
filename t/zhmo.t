use lib 'lib';
use Modern::Perl;
use Schema;
use Test::Database;
use Test::Mojo;
use Test::More tests => 3;

use FindBin;
require "$FindBin::Bin/../zhmo.pl";

my $dsn = 'db/test.db';
Test::Database->new->create(Schema => $dsn);
$ENV{ZHMO_DB} = 'dbi:SQLite:' . $dsn;

subtest 'Show login page' => sub {
    plan tests => 8;

    my $t = Test::Mojo->new;
    $t->ua->max_redirects(2);

    $t->get_ok('/')
        ->status_is(200)
        ->element_exists('html head meta[charset="utf-8"]')
        ->element_exists('html head title')
        ->element_exists('form input.form-control#email[type="text"]')
        ->element_exists('form '
            . 'input.form-control#password[type="password"]')
        ->element_exists('form div.checkbox label'
            . ' input#remember[type="checkbox"]')
        ->element_exists('form '
            . 'button.btn.btn-lg.btn-primary.btn-block[type="submit"]');
};

subtest 'Login with bad credentials' => sub {
    plan tests => 6;

    my $t = Test::Mojo->new;
    $t->ua->max_redirects(2);

    $t->post_ok('/login' => form => {
            email => 'vovka@vovka667.org',
            password => 'incorrect', })
        ->status_is(200)
        ->element_exists('form input.form-control#email[type="text"]');;
    $t->get_ok('/')->status_is(200)
        ->element_exists('form input.form-control#email[type="text"]');
};

subtest 'Login with good credentials' => sub {
    plan tests => 12;

    my $t = Test::Mojo->new;
    $t->ua->max_redirects(2);

    $t->post_ok('/login' => form => {
            email => 'vovka@vovka667.org',
            password => 'q1', })
        ->status_is(200)
        ->element_exists('html body div.list-group a.list-group-item')
        ->element_exists('html body table.table.table-bordered.table-striped');
    $t->get_ok('/')
        ->status_is(200)
        ->element_exists('html body div.list-group a.list-group-item')
        ->element_exists('html body table.table.table-bordered.table-striped');

    subtest 'Show accounts page' => sub {
        plan tests => 12;

        $t->get_ok('/account/1')
            ->status_is(200)
            ->element_exists('html body div.list-group a.list-group-item')
            ->element_exists('html body table.table.table-bordered.table-striped');

        $t->get_ok('/account/4')
            ->status_is(403);
        $t->get_ok('/account/-1')
            ->status_is(404);
        $t->get_ok('/account/invalid')
            ->status_is(404);
        $t->get_ok('/account')
            ->status_is(404);
    };

    subtest 'Check transactions' => sub {
        plan tests => 2;

        subtest 'Show new transaction from' => sub {
            plan tests => 10;

            $t->get_ok('/transaction/new')
                ->status_is(200)
                ->element_exists('html body div.list-group a.list-group-item')
                ->element_exists('form input.form-control#descr[type="text"]')
                ->element_exists('form input.form-control#sum[type="text"]')
                ->element_exists('form input.form-control#date[type="text"]')
                ->element_exists('form select.form-control#debit_id option')
                ->element_exists('form select.form-control#credit_id option')
                ->element_exists('form button.btn.btn-primary[type="submit"]')
                ->element_exists('form button.btn.btn-default[type="reset"]');
        };

        subtest 'Create new transaction' => sub {
            plan tests => 8;

            my $csrf_token = $t->tx->res->dom->at('input[name="csrf_token"]')
                ->attr('value');

            $t->post_ok('/transaction/new')
                ->status_is(400);
            $t->post_ok('/transaction/new' => form => {
                descr     => 'Pi',
                sum       => 3.14,
                date      => '',
                debit_id  => 1,
                credit_id => 2,
            })->status_is(403);
            $t->post_ok('/transaction/new' => form => {
                descr      => 'Pi',
                sum        => 3.14,
                date       => '',
                debit_id   => 1,
                credit_id  => 2,
                csrf_token => 'invalid',
            })->status_is(403);
            $t->post_ok('/transaction/new' => form => {
                descr      => 'Pi',
                sum        => 3.14,
                date       => '',
                debit_id   => 1,
                credit_id  => 2,
                csrf_token => $csrf_token,
            })->status_is(200);
        };
    };

    subtest 'Check accounts' => sub {
        plan tests => 2;

        subtest 'Show new account from' => sub {
            plan tests => 9;

            $t->get_ok('/account/new')
                ->status_is(200)
                ->element_exists('html body div.list-group a.list-group-item')
                ->element_exists('form input.form-control#name[type="text"]')
                ->element_exists('form select.form-control#type_id option')
                ->element_exists('form input.form-control#balance[type="text"]')
                ->element_exists('form select.form-control#currency_id option')
                ->element_exists('form button.btn.btn-primary[type="submit"]')
                ->element_exists('form button.btn.btn-default[type="reset"]');
        };

        subtest 'Create new account' => sub {
            plan tests => 8;

            my $csrf_token = $t->tx->res->dom->at('input[name="csrf_token"]')->attr('value');

            $t->post_ok('/account/new')
                ->status_is(400);
            $t->post_ok('/account/new' => form => {
                name        => 'Pie',
                balance     => 3.14,
                type_id     => 1,
                currency_id => 2,
            })->status_is(403);
            $t->post_ok('/account/new' => form => {
                name        => 'Pie',
                balance     => 3.14,
                type_id     => 1,
                currency_id => 2,
                csrf_token  => 'invalid',
            })->status_is(403);
            $t->post_ok('/account/new' => form => {
                name        => 'Pie',
                balance     => 3.14,
                type_id     => 1,
                currency_id => 2,
                csrf_token  => $csrf_token,
            })->status_is(200);
        };
    };

    subtest 'Common checks' => sub {
        plan tests => 8;

        # Несуществующая страница
        $t->get_ok('/ne_page')
            ->status_is(404);

        # Выход
        $t->get_ok('/exit')->status_is(200)
            ->element_exists('form input.form-control#email[type="text"]');;
        $t->get_ok('/')->status_is(200)
            ->element_exists('form input.form-control#email[type="text"]');
    };
};
