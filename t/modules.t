#!/usr/bin/env perl

use Modern::Perl;
use Test::More;

my @modules = (
    'Crypt::Eksblowfish::Bcrypt',
    'Crypt::OpenSSL::Random',
    'DBIx::Class',
    'lib/Schema.pm',
    'MIME::Base64',
    'Modern::Perl',
    'Mojolicious::Lite',
);

plan tests => scalar @modules;

for my $module (@modules) {
    require_ok($module);
}
