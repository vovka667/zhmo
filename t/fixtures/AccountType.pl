[
    AccountType => {
        id        => 1,
        name      => 'Дебетовая карта',
        external  => 0,
    },
    AccountType => {
        id        => 2,
        name      => 'Кредитная карта',
        external  => 0,
    },
    AccountType => {
        id        => 3,
        name      => 'Депозит',
        external  => 0,
    },
    AccountType => {
        id        => 4,
        name      => 'Наличные',
        external  => 0,
    },
    AccountType => {
        id        => 9,
        name      => 'Актив',
        external  => 1,
    },
    AccountType => {
        id        => 10,
        name      => 'Пасив',
        external  => 1,
    },
]
