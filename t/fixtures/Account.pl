[
    Account => {
        id              => 1,
        name            => 'Зарплатная карта',
        account_type_id => 1,
        balance         => 1500000,
        user_id         => 1,
        currency_id     => 3,
    },
    Account => {
        id              => 2,
        name            => 'Заначка',
        account_type_id => 3,
        balance         => 1500000000,
        user_id         => 1,
        currency_id     => 2,
    },
    Account => {
        id              => 3,
        name            => 'Зарплата',
        account_type_id => 4,
        balance         => 0,
        user_id         => 1,
        currency_id     => 3,
    },
    Account => {
        id              => 4,
        name            => 'Зарплатная карта',
        account_type_id => 1,
        balance         => 1500000,
        user_id         => 2,
        currency_id     => 3,
    },
]
