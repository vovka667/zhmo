[
    Transaction => {
        id                  => 1,
        debit_id            => 1,
        d_sum               => 100,
        credit_id           => 2,
        c_sum               => 100,
        transaction_type_id => 2,
        time                => 1,
        descr               => '10% На покупку квартиры',
    },
    Transaction => {
        id                  => 2,
        debit_id            => 3,
        d_sum               => 1000,
        credit_id           => 1,
        c_sum               => 1000,
        transaction_type_id => 1,
        time                => 0,
        descr               => 'Пришла зпшечка за 70й год',
    },
]
