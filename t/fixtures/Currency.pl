[
    Currency => {
        id   => 1,
        name => 'Доллар США',
        iso  => 'USD',
        sign => '$',
    },
    Currency => {
        id   => 2,
        name => 'Евро',
        iso  => 'EUR',
        sign => '€',
    },
    Currency => {
        id   => 3,
        name => 'Российский рубль',
        iso  => 'RUB',
        sign => 'р.',
    },
]
