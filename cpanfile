requires 'Crypt::Eksblowfish::Bcrypt';
requires 'DBIx::Class';
requires 'MIME::Base64';
requires 'Modern::Perl';
requires 'Mojolicious::Lite';
requires 'Mojolicious', '>= 4.63';

recommends 'Crypt::OpenSSL::Random';

test_requires 'Test::More';
test_requires 'Test::Mojo';
test_requires 'File::Slurp';
test_requires 'SQL::Translator';
